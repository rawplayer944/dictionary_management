const validate = require("validate.js");

export const validateDictionary = (dictionary, dictionaries) => {
  //errors init
  let indicate = [];
  let errors = {
    text: { nameAmbigous: "", nameEmpty: "", emptyRecords: "" },
    signal: {
      weak: { ambigousRange: [], duplicateRecords: [] },
      strong: {
        chainedRecords: []
      }
    }
  };

  //name is empty?
  if (validate.isEmpty(dictionary.name)) {
    errors.text.nameEmpty = "Dictionary name must not be empty.";
    indicate.push("emptyName");
  }

  //name already used?
  const names = [];
  const ids = [];
  dictionaries.forEach(dictionary => {
    ids.push(dictionary.id.toLowerCase());
    names.push(dictionary.name.toLowerCase());
  });

  const prevDictIndex = ids.findIndex(id => {
    return dictionary.id === id;
  });

  if (
    (validate.contains(names, dictionary.name.toLowerCase()) &&
      !validate.contains(ids, dictionary.id.toLowerCase())) ||
    (prevDictIndex !== -1 &&
      validate.contains(ids, dictionary.id.toLowerCase()) &&
      dictionaries[prevDictIndex].name.toLowerCase() !==
        dictionary.name.toLowerCase() &&
      validate.contains(names, dictionary.name.toLowerCase()))
  ) {
    errors.text.nameAmbigous = "Dictionary name is already in use.";
    indicate.push("usedName");
  }

  //any record empty?
  if (
    dictionary.records.filter(record => {
      return validate.isEmpty(record.domain) || validate.isEmpty(record.range)
        ? true
        : false;
    }).length > 0
  ) {
    errors.text.emptyRecords = "Records must not be empty.";
    indicate.push("emptyRecords");
  }

  if (!indicate.includes("emptyRecords")) {
    //is a range among domains?
    let domains = [];
    let ranges = [];
    let chainIndexes = [];

    dictionary.records.forEach(record => {
      domains.push(record.domain.toLowerCase());
      ranges.push(record.range.toLowerCase());
    });

    domains.forEach((domain, index) => {
      if (
        ranges.includes(domain.toLowerCase()) &&
        ranges[index] !== domains[index]
      ) {
        chainIndexes.push(index);
      }
    });

    if (chainIndexes.length !== 0) {
      errors.signal.strong.chainedRecords = chainIndexes;
      indicate.push("chainedRecords");
    }

    //same range as domain more than twice?

    // let ambigousRangeIndexes = [];
    // if (
    //   dictionary.records.filter((record, index) => {
    //     if (record.domain.toLowerCase() === record.range.toLowerCase()) {
    //       ambigousRangeIndexes.push(index);
    //       return true;
    //     }
    //     return false;
    //   }).length > 1
    // ) {
    //   errors.signal.weak.ambigousRange = ambigousRangeIndexes;
    //   indicate.push("ambiogousRange");
    // }

    //two or more same rows?
    let already = [];
    let duplicateIndexes = [];

    dictionary.records.forEach((record, index) => {
      if (already.length > 0) {
        if (
          already.filter(alreadyRecord => {
            if (
              alreadyRecord.domain.toLowerCase() === record.domain.toLowerCase()
            ) {
              return true;
            } else {
              return false;
            }
          }).length > 0
        ) {
          duplicateIndexes.push(index);
        }
      }

      already.push(record);
    });
    if (duplicateIndexes.length !== 0) {
      errors.signal.weak.duplicateRecords = duplicateIndexes;
      indicate.push("duplicateRecords");
    }

    return {
      errors: errors,
      isValid:
        validate.isEmpty(indicate) ||
        (!indicate.includes("emptyRecords") &&
          !indicate.includes("usedName") &&
          !indicate.includes("emptyName"))
    };
  }
};
