import { GET_ERRORS, CLEAN_ERRORS } from "../actions/types";

const initialState = {
  errors: {}
};

export function errorReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    case CLEAN_ERRORS:
      return {
        errors: {}
      };
    default:
      return state;
  }
}
