import {
  GET_DICTIONARIES,
  GET_DICTIONARY,
  LOADING,
  SET_DICTIONARIES,
  CLEAR_CURRENT_DICTIONARY
} from "../actions/types";

const initialState = {
  dictionaries: [],
  dictionary: {},
  loading: false
};

export function dictionaryReducer(state = initialState, action) {
  switch (action.type) {
    case GET_DICTIONARY:
      return {
        ...state,
        dictionary: action.payload,
        loading: false
      };
    case GET_DICTIONARIES:
      return {
        ...state,
        dictionaries: action.payload,
        loading: false
      };
    case LOADING:
      return {
        ...state,
        loading: true
      };
    case SET_DICTIONARIES:
      return {
        ...state
      };

    case CLEAR_CURRENT_DICTIONARY:
      return {
        ...state,
        dictionary: {}
      };
    default:
      return state;
  }
}
