import { SWITCH_MODE } from "../actions/types";

const initialState = {
  isEditing: false
};

export function navigationReducer(state = initialState, action) {
  switch (action.type) {
    case SWITCH_MODE:
      return {
        ...state,
        isEditing: !state.isEditing
      };
    default:
      return state;
  }
}
