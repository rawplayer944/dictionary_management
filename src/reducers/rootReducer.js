import { combineReducers } from "redux";
import { dictionaryReducer } from "./dictionaryReducer";
import { navigationReducer } from "./navigationReducer";
import { errorReducer } from "./errorReducer";

export default combineReducers({
  dictionaries: dictionaryReducer,
  navigation: navigationReducer,
  errors: errorReducer
});
