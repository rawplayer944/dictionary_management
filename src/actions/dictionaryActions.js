import {
  GET_DICTIONARIES,
  SET_DICTIONARIES,
  LOADING,
  GET_DICTIONARY,
  CLEAR_CURRENT_DICTIONARY,
  GET_ERRORS,
  CLEAN_ERRORS
} from "./types";
import { validateDictionary } from "../validation/dictionaryValidator";
import { storageHash } from "../config/keys";

export const getDictionaries = () => dispatch => {
  dispatch(setLoading());
  const dictionariesString = localStorage.getItem(storageHash);
  const dictionaries = JSON.parse(dictionariesString);

  dispatch({
    type: GET_DICTIONARIES,
    payload: dictionaries
  });
};

export const setDictionaries = dictionaries => dispatch => {
  const dictionariesArray = dictionaries;
  const dictionariesString = JSON.stringify(dictionariesArray);
  localStorage.setItem(storageHash, dictionariesString);
  dispatch({
    type: SET_DICTIONARIES
  });
};

export const clearCurrentDictionary = () => dispatch => {
  dispatch({
    type: CLEAR_CURRENT_DICTIONARY
  });
};

export const getDictionary = (
  dictionaries,
  dictionaryId,
  history
) => dispatch => {
  dispatch(setLoading);
  dictionaries.forEach((dictionary, index) => {
    if (dictionary.id === dictionaryId) {
      const foundDictionary = dictionaries[index];
      dispatch({
        type: GET_DICTIONARY,
        payload: foundDictionary
      });
    }
  });
  history.push("/edit");
};

export const addDictionary = (dictionary, dictionaries) => dispatch => {
  dispatch({
    type: CLEAN_ERRORS
  });
  const { errors, isValid } = validateDictionary(dictionary, dictionaries);
  if (isValid) {
    let dictionariesArray;
    const index = dictionaries.findIndex(dictionaryEl => {
      return dictionaryEl.id === dictionary.id;
    });

    if (index !== -1) {
      dictionaries[index] = { ...dictionary, ...errors };
      dictionariesArray = dictionaries;
    } else {
      dictionariesArray = [...dictionaries, { ...dictionary, ...errors }];
    }

    const dictionariesArrayStringified = JSON.stringify(dictionariesArray);
    localStorage.setItem(storageHash, dictionariesArrayStringified);
    dispatch(getDictionaries());
    if (
      errors.signal.weak.ambigousRange.length > 0 ||
      errors.signal.weak.duplicateRecords.length > 0 ||
      errors.signal.strong.chainedRecords.length > 0
    ) {
      window.showAlert("saved_with_warning");
    } else {
      window.showAlert("saved");
    }
    return true;
  } else {
    dispatch({
      type: GET_ERRORS,
      payload: errors
    });

    window.showModal("errorsModal");
  }
};

export const removeDictionary = (dictionaryId, dictionaries) => dispatch => {
  dictionaries.forEach((dictionary, index) => {
    if (dictionaryId === dictionary.id) {
      dictionaries.splice(index, 1);
    }
  });
  dispatch(setDictionaries(dictionaries));
  dispatch(getDictionaries());
};

export const setLoading = () => {
  return {
    type: LOADING
  };
};
