import { SWITCH_MODE } from "./types";

export const switchMode = () => dispatch => {
  dispatch({ type: SWITCH_MODE });
};
