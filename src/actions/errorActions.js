import { CLEAN_ERRORS } from "./types";

export const clearErrors = () => dispatch => {
  dispatch({
    type: CLEAN_ERRORS
  });
};
