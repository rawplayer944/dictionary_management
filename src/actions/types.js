export const DELETE_DICTIONARY = "DELETE_DICTIONARY";
export const ADD_DICTIONARY = "ADD_DICTIONARY";
export const GET_DICTIONARIES = "GET_DICTIONARIES";
export const GET_DICTIONARY = "GET_DICTIONARY";
export const SET_DICTIONARIES = "SET_DICTIONARIES";
export const LOADING = "LOADING";
export const SWITCH_MODE = "SWITCH_MODE";
export const CLEAR_CURRENT_DICTIONARY = "CLEAR_CURRENT_DICTIONARY";
export const GET_ERRORS = "GET_ERRORS";
export const CLEAN_ERRORS = "CLEAN_ERRORS";
