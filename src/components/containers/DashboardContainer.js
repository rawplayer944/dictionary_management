import React, { Component } from "react";
import {
  getDictionaries,
  removeDictionary,
  getDictionary
} from "../../actions/dictionaryActions";
import { switchMode } from "../../actions/navigationActions";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import Dashboard from "../representationals/Dashboard";
import { storageHash } from "../../config/keys";
import { validateDictionary } from "../../validation/dictionaryValidator";

class DashboardContainer extends Component {
  constructor(props) {
    super(props);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.loadSample = this.loadSample.bind(this);
  }

  loadSample() {
    const uniqid = require("uniqid");
    const domainColors = [
      "Garden Green",
      "Emerald Green",
      "Zephyre Blue",
      "Indigo Blue",
      "Ruby Red",
      "Obsidian Black",
      "Violet White"
    ];
    const rangeColors = [
      "Green",
      "Blue",
      "Red",
      "Black",
      "Garden Green",
      "Emerald Green",
      "Zephyre Blue",
      "Indigo Blue",
      "Ruby Red",
      "Obsidian Black",
      "Violet White"
    ];
    const dictionariesSampleDataPrepare = [];
    const dictionariesSampleData = [];
    for (let l = 0; l < 12; l++) {
      const id = uniqid();
      const uniqName = `Sample ${l + 1}`;
      let recordRows = [];
      for (let k = 0; k < 6; k++) {
        let domainIntegerNum = Math.floor(Math.random() * 6);
        let rangeIntegerNum = Math.floor(Math.random() * 10);
        recordRows.push({
          domain: domainColors[domainIntegerNum],
          range: rangeColors[rangeIntegerNum]
        });
      }
      dictionariesSampleDataPrepare.push({
        records: recordRows,
        id: id,
        name: uniqName
      });
    }

    dictionariesSampleDataPrepare.forEach(dictionarySample => {
      const { errors } = validateDictionary(
        dictionarySample,
        dictionariesSampleData
      );

      dictionarySample = { ...dictionarySample, ...errors };
      dictionariesSampleData.push(dictionarySample);
    });
    console.log(dictionariesSampleData);
    localStorage.setItem(storageHash, JSON.stringify(dictionariesSampleData));
    this.props.getDictionaries();
  }

  // });

  componentWillMount() {
    this.props.getDictionaries();
  }

  componentDidMount() {
    //window.tooltip("signal-tooltip");
  }

  componentWillUnmount() {
    window.tooltip("signal-tooltip");
  }

  handleEditClick(dictionaryId) {
    this.props.getDictionary(
      this.props.dictionaries.dictionaries,
      dictionaryId,
      this.props.history
    );
    this.props.switchMode();
  }

  handleDeleteClick(dictionaryId) {
    if (
      window.confirm(
        "Are You sure, that You would like to delete this dictionary? It cannot be undone."
      )
    ) {
      this.props.removeDictionary(
        dictionaryId,
        this.props.dictionaries.dictionaries
      );
    } else {
      return false;
    }
  }

  render() {
    const { dictionaries } = this.props.dictionaries;
    return (
      <Dashboard
        dictionaries={dictionaries}
        handleDeleteClick={this.handleDeleteClick}
        handleEditClick={this.handleEditClick}
        handleSwitch={this.props.switchMode}
        loadSample={this.loadSample}
      />
    );
  }
}

const mapStateToProps = state => ({
  dictionaries: state.dictionaries,
  loading: state.loading
});

DashboardContainer.propTypes = {
  dictionaries: PropTypes.object,
  loading: PropTypes.bool
};

export default connect(
  mapStateToProps,
  { getDictionaries, removeDictionary, getDictionary, switchMode }
)(withRouter(DashboardContainer));
