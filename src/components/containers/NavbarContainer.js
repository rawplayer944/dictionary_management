import React, { Component } from "react";
import PropTypes from "prop-types";
import Navbar from "../representationals/Navbar";
import { connect } from "react-redux";
import { switchMode } from "../../actions/navigationActions";
import {
  clearCurrentDictionary,
  getDictionaries
} from "../../actions/dictionaryActions";
import { clearErrors } from "../../actions/errorActions";

class NavbarContainer extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    if (
      window.location.pathname === "/edit" &&
      !this.props.navigation.isEditing
    ) {
      this.props.switchMode();
    }
    this.props.getDictionaries();
  }

  handleClick() {
    this.props.clearCurrentDictionary();
    this.props.getDictionaries();
    this.props.clearErrors();
    this.props.switchMode();
  }

  render() {
    const { isEditing } = this.props.navigation;
    return (
      <Navbar
        handleClick={this.handleClick}
        isEditing={isEditing}
        isBottomFixed={this.props.isBottomFixed}
        dictionary={this.props.dictionary}
      />
    );
  }
}

const mapStateToProps = state => ({
  navigation: state.navigation,
  dictionary: state.dictionaries.dictionary
});

NavbarContainer.propTypes = {
  switchMode: PropTypes.func.isRequired,
  clearCurrentDictionary: PropTypes.func.isRequired,
  getDictionaries: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired,
  navigaton: PropTypes.object,
  dictionary: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps,
  {
    switchMode,
    clearCurrentDictionary,
    getDictionaries,
    clearErrors
  }
)(NavbarContainer);
