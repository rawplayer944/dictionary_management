import React, { Component } from "react";
import { connect } from "react-redux";
import Edit from "../representationals/Edit";
import {
  addDictionary,
  getDictionaries
} from "../../actions/dictionaryActions";
import { clearErrors } from "../../actions/errorActions";
import PropTypes from "prop-types";
class EditContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      records: [{ domain: "", range: "" }],
      name: "",
      id: "",
      errors: {}
    };
    this.handleAddNewRecord = this.handleAddNewRecord.bind(this);
    this.handleRemoveLastRecord = this.handleRemoveLastRecord.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAddNewDictionary = this.handleAddNewDictionary.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.newRecordOnEnter = this.newRecordOnEnter.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      Object.keys(nextProps.dictionaries.dictionary).length > 0 &&
      prevState.loaded !== true
    ) {
      return {
        ...prevState,
        records: nextProps.dictionaries.dictionary.records,
        name: nextProps.dictionaries.dictionary.name,
        id: nextProps.dictionaries.dictionary.id,
        loaded: true
      };
    }
    if (Object.keys(nextProps.errors.errors).length >= 0) {
      return { ...prevState, errors: nextProps.errors.errors };
    } else {
      return null;
    }
  }

  newRecordOnEnter(e) {
    if (
      e.which === 13 &&
      e.target.classList.contains("enter-next") &&
      Number(e.target.dataset.id) === this.state.records.length - 1
    ) {
      this.handleAddNewRecord(e);
    }
  }

  componentDidMount() {
    document.getElementById("name").focus();
    window.addEventListener("keydown", this.newRecordOnEnter);
    //window.tooltip("button-tooltip");
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.newRecordOnEnter);
    window.tooltip("button-tooltip");
  }

  handleReset() {
    if (this.state.records.length > 3) {
      if (
        !window.confirm(
          "There are more than 3 records typed in. Are You sure, that You want to reset the whole form?"
        )
      )
        return false;
    }

    this.setState({
      records: [{ domain: "", range: "" }],
      name: ""
    });
    this.props.clearErrors();
  }

  handleChange(e) {
    if (["domain", "range"].includes(e.target.dataset.class)) {
      let records = [...this.state.records];
      records[e.target.dataset.id][e.target.dataset.class] = e.target.value;
      this.setState({ records });
    } else {
      this.setState({
        [e.target.name]: e.target.value
      });
    }
  }

  handleAddNewDictionary() {
    if (
      this.state.id !== "" &&
      !window.confirm(
        `Are You sure that You would like to overwrite the dictionary You've been editing so far?`
      )
    )
      return false;
    const sanitizeHtml = require("sanitize-html");
    const uniqid = require("uniqid");
    const { dictionaries } = this.props.dictionaries;
    const newDictionary = {};
    //TODO validation
    if (this.state.id === "") {
      newDictionary.id = uniqid();
    } else {
      newDictionary.id = this.state.id;
    }
    newDictionary.name = sanitizeHtml(this.state.name.trim(), {
      allowedAttributes: [],
      allowedTags: []
    });
    newDictionary.records = this.state.records.map(record => {
      return {
        domain: sanitizeHtml(record.domain.trim(), {
          allowedAttributes: [],
          allowedTags: []
        }),
        range: sanitizeHtml(record.range.trim(), {
          allowedAttributes: [],
          allowedTags: []
        })
      };
    });

    if (this.props.addDictionary(newDictionary, dictionaries) === true) {
      this.setState({
        records: [{ domain: "", range: "" }],
        name: "",
        id: "",
        errors: {}
      });
    }
  }

  handleAddNewRecord(e) {
    e.preventDefault();

    this.setState(prevState => {
      console.log([...prevState.records, { domain: "", range: "" }]);
      return { records: [...prevState.records, { domain: "", range: "" }] };
    });
  }

  handleRemoveLastRecord(e) {
    e.preventDefault();
    this.setState(prevState => {
      if (prevState.records.length === 1) return false;
      prevState.records.pop();
      return { records: prevState.records };
    });
  }

  render() {
    const { records, name } = this.state;
    const { errors } = this.props.errors;
    return (
      <Edit
        handleAddNewRecord={this.handleAddNewRecord}
        handleRemoveLastRecord={this.handleRemoveLastRecord}
        handleAddNewDictionary={this.handleAddNewDictionary}
        nameValue={name}
        handleReset={this.handleReset}
        records={records}
        handleChange={this.handleChange}
        textErrors={errors.text}
        signalErrors={errors.signal}
      />
    );
  }
}

EditContainer.propTypes = {
  dictionaries: PropTypes.object,
  errors: PropTypes.object,
  addDictionary: PropTypes.func,
  getDictionaries: PropTypes.func,
  clearErrors: PropTypes.func
};

const mapStateToProps = state => ({
  dictionaries: state.dictionaries,
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { addDictionary, getDictionaries, clearErrors }
)(EditContainer);
