import React from "react";
import PropTypes from "prop-types";

export default function FormGroup({ nameValue }) {
  return (
    <div className="row">
      <div className="col-12 px-5">
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control rounded-0"
            id="name"
            name="name"
            aria-describedby="nameHelp"
            placeholder="Name..."
            value={nameValue}
          />
          <small id="nameHelp" className="form-text text-muted">
            This will be the name of your dictionary.
          </small>
        </div>
      </div>
    </div>
  );
}

FormGroup.propTypes = {
  nameValue: PropTypes.string
};
