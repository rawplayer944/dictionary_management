import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
export default function Jumbotron({ handleSwitch, loadSample }) {
  return (
    <div className="jumbotron w-100 rounded-0">
      <h1 className="display-4">Hi!</h1>
      <p className="lead">It seems You have not added a dictionary yet.</p>
      <hr className="my-4" />
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-4">
            <Link
              onClick={handleSwitch}
              className="btn btn-primary btn-lg btn-block rounded-0 "
              to="/edit"
              role="button"
            >
              Create new
            </Link>
          </div>
          <div className="col-lg-4">
            <h3 className="text-center">or</h3>
          </div>
          <div className="col-lg-4">
            <button
              onClick={loadSample}
              className="btn btn-secondary  btn-lg rounded-0 btn-block"
            >
              Load random data
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

Jumbotron.propTypes = {
  handleSwitch: PropTypes.func
};
