import React from "react";
import PropTypes from "prop-types";

export default function InputGroup({
  domainId,
  rangeId,
  record,
  dataId,
  signalErrors,
  textErrors
}) {
  const signal = () => {
    if (signalErrors && textErrors) {
      if (
        textErrors.nameAmbigous === "" &&
        textErrors.nameEmpty === "" &&
        textErrors.emptyRecords === ""
      ) {
        if (
          signalErrors.strong.chainedRecords.length > 0 &&
          signalErrors.strong.chainedRecords.includes(dataId)
        ) {
          return (
            <div
              data-toggle="tooltip"
              data-placement="left"
              title="Chained records."
            >
              <i className="far fa-sad-cry text-danger" />
            </div>
          );
        } else if (
          signalErrors.strong.chainedRecords.length > 0 &&
          !signalErrors.strong.chainedRecords.includes(dataId) &&
          (signalErrors.weak.ambigousRange.includes(dataId) ||
            signalErrors.weak.duplicateRecords.includes(dataId))
        ) {
          return (
            <div
              data-toggle="tooltip"
              data-placement="left"
              title="Duplicate records, or same domain as range."
            >
              <i className="far fa-frown text-warning" />
            </div>
          );
        } else if (
          (signalErrors.strong.chainedRecords.length === 0 &&
            signalErrors.weak.ambigousRange.length > 0 &&
            signalErrors.weak.ambigousRange.includes(dataId)) ||
          (signalErrors.strong.chainedRecords.length === 0 &&
            signalErrors.weak.duplicateRecords.length > 0 &&
            signalErrors.weak.duplicateRecords.includes(dataId))
        ) {
          return (
            <div
              data-toggle="tooltip"
              data-placement="left"
              title="Duplicate records, or same domain as range."
            >
              <i className="far fa-frown text-warning" />
            </div>
          );
        } else {
          return (
            <div data-toggle="tooltip" data-placement="left" title="Seems OK.">
              <i className="fas fa-check text-success" />
            </div>
          );
        }
      } else {
        return dataId + 1;
      }
    } else {
      return dataId + 1;
    }
  };

  return (
    <div className="row my-4">
      <div className="col-12 px-5">
        <div className="input-group rounded-0">
          <div className="input-group-prepend">
            <span className="input-group-text">{signal()}</span>
          </div>
          <input
            type="text"
            className="form-control rounded-0"
            data-class="domain"
            id={domainId}
            data-id={dataId}
            value={record["domain"]}
            placeholder={`#${dataId + 1} Domain...`}
          />
          <input
            type="text"
            className="form-control rounded-0 enter-next"
            data-class="range"
            id={rangeId}
            data-id={dataId}
            value={record["range"]}
            placeholder={`#${dataId + 1} Range...`}
          />
        </div>
      </div>
    </div>
  );
}

InputGroup.propTypes = {
  domainId: PropTypes.string,
  rangeId: PropTypes.string,
  record: PropTypes.object,
  dataId: PropTypes.number,
  signalErrors: PropTypes.object,
  textErrors: PropTypes.object
};
