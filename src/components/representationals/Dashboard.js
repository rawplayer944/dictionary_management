import React from "react";
import DictionaryTable from "./DictionaryTable";
import Jumbotron from "../common/Jumbotron";
import PropTypes from "prop-types";

export default function Dashboard({
  dictionaries,
  handleEditClick,
  handleDeleteClick,
  handleSwitch,
  loadSample
}) {
  const styles = {};
  styles.dashboard = { marginTop: 80 };

  return (
    <div id="dashboard" style={styles.dashboard}>
      <div className="container py-5">
        <div className="row">
          {dictionaries && dictionaries.length === 0 && (
            <Jumbotron handleSwitch={handleSwitch} loadSample={loadSample} />
          )}
          {dictionaries &&
            dictionaries.length !== 0 &&
            dictionaries.map(dictionary => {
              return (
                <DictionaryTable
                  handleDeleteClick={handleDeleteClick}
                  handleEditClick={handleEditClick}
                  dictionary={dictionary}
                  key={dictionary.id}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
}

Dashboard.propTypes = {
  dictionaries: PropTypes.array,
  handleEditClick: PropTypes.func,
  handleDeleteClick: PropTypes.func,
  handleSwitch: PropTypes.func
};
