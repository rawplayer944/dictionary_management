import React from "react";
import InputGroup from "../common/InputGroup";
import FormGroup from "../common/FormGroup";
import PropTypes from "prop-types";

function Edit({
  handleRemoveLastRecord,
  handleAddNewRecord,
  handleAddNewDictionary,
  handleReset,
  textErrors,
  signalErrors,
  records,
  handleChange,
  nameValue
}) {
  const styles = {};
  styles.edit = {
    marginTop: 80,
    marginBottom: 80,
    overflow: "auto"
  };
  styles.controlButtonsMiddle = {
    minWidth: 40
  };
  styles.controlButtonsSide = {
    minWidth: 60
  };

  styles.navbarBottom = {
    maxHeight: 80
  };

  return (
    <div id="edit" style={styles.edit} className="container">
      <div className="row">
        <div className="col-12">
          <div
            className="alert alert-success alert-dismissible collapse my-2 mx-4"
            role="alert"
            id="saved"
          >
            <b>Great!</b> Dictionary has been successfully saved!
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div
            className="alert alert-warning alert-dismissible collapse my-2 mx-4"
            role="alert"
            id="saved_with_warning"
          >
            <b>Warning!</b> Dictionary has been saved with improper structure!
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form className="my-5" onChange={handleChange}>
            <FormGroup nameValue={nameValue} />
            <hr className="mb-2" />
            {records.map((record, index) => {
              let domainId = `domain-${index}`,
                rangeId = `range-${index}`;
              return (
                <InputGroup
                  key={index}
                  record={record}
                  domainId={domainId}
                  rangeId={rangeId}
                  dataId={index}
                  signalErrors={signalErrors}
                  textErrors={textErrors}
                />
              );
            })}
          </form>
          <div
            className="modal fade"
            id="errorsModal"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="errors"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content rounded-0">
                <div className="modal-header bg-warning rounded-0">
                  <h5 className="modal-title" id="errors">
                    Errors
                  </h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <ul>
                    {textErrors &&
                      Object.keys(textErrors).map(key => {
                        if (textErrors[key] !== "") {
                          return <li key={key}> {textErrors[key]}</li>;
                        } else {
                          return null;
                        }
                      })}
                  </ul>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-primary rounded-0"
                    data-dismiss="modal"
                  >
                    OK
                  </button>
                </div>
              </div>
            </div>
          </div>
          <nav
            className="navbar navbar-light bg-light fixed-bottom"
            style={styles.navbarBottom}
          >
            <div className="container-fluid">
              <div className="col-6 d-flex justify-content-between">
                <button
                  className="btn btn-dark btn-sm mx-1 rounded-0 button-tooltip"
                  type="button"
                  style={styles.controlButtonsSide}
                  onClick={handleReset}
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Reset form."
                >
                  <i className="fas fa-ban" />
                </button>
                <button
                  className="btn btn-warning btn-sm mx-1 rounded-0 button-tooltip"
                  type="button"
                  style={styles.controlButtonsMiddle}
                  onClick={handleRemoveLastRecord}
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Remove last row."
                >
                  <i className="fas fa-minus" />
                </button>
              </div>
              <div className="col-6 d-flex justify-content-between">
                <button
                  className="btn btn-secondary btn-sm mx-1 rounded-0 button-tooltip"
                  type="button"
                  style={styles.controlButtonsMiddle}
                  onClick={handleAddNewRecord}
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add row."
                >
                  <i className="fas fa-plus" />
                </button>

                <button
                  className="btn btn-success btn-sm mx-1 rounded-0 button-tooltip"
                  type="button"
                  style={styles.controlButtonsSide}
                  onClick={handleAddNewDictionary}
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Save/overwrite."
                >
                  <i className="fas fa-save" />
                </button>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
}

Edit.propTypes = {
  handleRemoveLastRecord: PropTypes.func,
  handleAddNewRecord: PropTypes.func,
  handleAddNewDictionary: PropTypes.func,
  handleReset: PropTypes.func,
  textErrors: PropTypes.object,
  signalErrors: PropTypes.object,
  records: PropTypes.array,
  handleChange: PropTypes.func,
  nameValue: PropTypes.string
};

export default Edit;
