import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

export default function Navbar({ isEditing, handleClick, dictionary }) {
  const styles = {};
  styles.nav = {
    display: "flex",
    justifyContent: "space-between",
    minHeight: 80
  };
  styles.button = { width: 200 };

  return (
    <nav
      className="navbar navbar-expand-lg navbar-light bg-light fixed-top"
      style={styles.nav}
      id="navbar"
    >
      <div className="navbar-brand m-0" id="navbar_brand">
        {isEditing && Object.keys(dictionary).length > 0 ? (
          <h4 className="d-inline m-0">Editing</h4>
        ) : isEditing && Object.keys(dictionary).length === 0 ? (
          <h4 className="d-inline m-0" id="navbar_brand">
            Creating new
          </h4>
        ) : (
          <h4 className="d-inline m-0" id="navbar_brand">
            Dashboard
          </h4>
        )}
      </div>
      <Link
        to={isEditing ? "/" : "/edit"}
        className="navbar-brand m-0"
        onClick={handleClick}
      >
        <button
          className="btn btn-primary rounded-0"
          id="navigate"
          style={styles.button}
        >
          {isEditing ? (
            <>
              <h6 className="d-inline m-0">Browse</h6>{" "}
              <i className="fas fa-search" />
            </>
          ) : (
            <>
              <h6 className="d-inline m-0">Create new</h6>{" "}
              <i className="fas fa-plus" />
            </>
          )}
        </button>
      </Link>
    </nav>
  );
}

Navbar.propTypes = {
  isEditing: PropTypes.bool,
  handleClick: PropTypes.func,
  dictionary: PropTypes.object
};
