import React from "react";
import PropTypes from "prop-types";

export default function DictionaryTable({
  dictionary,
  handleEditClick,
  handleDeleteClick
}) {
  const styles = {};
  styles.cardHeader = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  };

  styles.card = {
    borderRadius: 0
  };

  styles.counter = {
    textAlign: "center"
  };

  styles.name = {
    display: "inline"
  };

  styles.badge = {
    margin: "0px 10px"
  };

  const showErrorBadge = () => {
    if (dictionary.signal) {
      if (dictionary.signal.strong.chainedRecords.length > 0) {
        return (
          <span
            className="badge badge-danger"
            style={styles.badge}
            onClick={() => handleEditClick(dictionary.id)}
          >
            Needs fix
          </span>
        );
      } else if (
        dictionary.signal.weak.duplicateRecords.length > 0 ||
        dictionary.signal.weak.ambigousRange.length > 0
      ) {
        return (
          <span
            className="badge badge-warning"
            style={styles.badge}
            onClick={() => handleEditClick(dictionary.id)}
          >
            Needs polishing
          </span>
        );
      } else {
        return (
          <span className="badge badge-success" style={styles.badge}>
            OK
          </span>
        );
      }
    }
  };

  const showErrors = index => {
    if (dictionary.signal) {
      if (
        dictionary.signal.strong.chainedRecords.length > 0 &&
        dictionary.signal.strong.chainedRecords.includes(index)
      ) {
        return (
          <i
            data-toggle="tooltip"
            data-placement="right"
            title="Chained or infinitely cycled records."
            className="far fa-sad-cry text-danger signal-tooltip"
          />
        );
      } else if (
        (dictionary.signal.weak.ambigousRange.length > 0 ||
          dictionary.signal.weak.duplicateRecords.length > 0) &&
        (dictionary.signal.weak.ambigousRange.includes(index) ||
          dictionary.signal.weak.duplicateRecords.includes(index))
      ) {
        return (
          <i
            data-toggle="tooltip"
            data-placement="right"
            title="Duplicate records or forks."
            className="far fa-frown text-warning signal-tooltip"
          />
        );
      } else {
        return (
          <i
            className="fas fa-check text-success signal-tooltip"
            data-toggle="tooltip"
            data-placement="right"
            title="This record is OK."
          />
        );
      }
    }
  };

  return (
    <div className="col-12 my-2">
      <div className="card border-dark" style={styles.card}>
        <div className="card-header" style={styles.cardHeader}>
          <div>
            <h5
              className="mb-0 link"
              style={styles.name}
              onClick={e => {
                e.stopPropagation();
                const collapsible = document.getElementById(dictionary.id);

                if (collapsible.classList.contains("show")) {
                  collapsible.classList.remove("show");
                } else {
                  collapsible.classList.add("show");
                }
              }}
            >
              {dictionary.name}
            </h5>

            {showErrorBadge()}
          </div>
          <div className="dropdown" style={styles.caret}>
            <button
              className="btn btn-link dropdown-toggle"
              id={dictionary.name}
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />

            <div className="dropdown-menu" aria-labelledby={dictionary.name}>
              <button
                className="dropdown-item btn btn-link"
                onClick={() => handleEditClick(dictionary.id)}
              >
                Edit
              </button>
              <button
                className="dropdown-item btn btn-link text-danger"
                onClick={() => handleDeleteClick(dictionary.id)}
              >
                Delete
              </button>
            </div>
          </div>
        </div>

        <div id={dictionary.id} className="collapse">
          <div className="card-body">
            <table className="table table-borderless table-hover">
              <thead>
                <tr>
                  <th scope="col" style={styles.counter}>
                    <i className="fas fa-list-ol" />
                  </th>
                  <th scope="col">Domain</th>
                  <th scope="col">Range</th>
                  <th scope="col">Status</th>
                </tr>
              </thead>
              <tbody>
                {dictionary.records.map((record, index) => {
                  return (
                    <tr key={index}>
                      <td style={styles.counter}>{index + 1}</td>
                      <td>{record.domain}</td>
                      <td>{record.range}</td>
                      <td>{showErrors(index)}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

DictionaryTable.propTypes = {
  dictionary: PropTypes.object,
  handleEditClick: PropTypes.func,
  handleDeleteClick: PropTypes.func
};
