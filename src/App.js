import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import { storageHash } from "./config/keys";
import DashboardContainer from "./components/containers/DashboardContainer";
import EditContainer from "./components/containers/EditContainer";
import NavbarContainer from "./components/containers/NavbarContainer";
import "./App.css";

class App extends Component {
  componentWillMount() {
    if (localStorage.getItem(storageHash) === null) {
      localStorage.setItem(storageHash, JSON.stringify([]));
    }
  }

  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div id="app">
            <NavbarContainer />
            <div id="app_content">
              <Route exact path="/" component={DashboardContainer} />
              <Route exact path="/edit" component={EditContainer} />
            </div>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
