#Dictionary management app#

## Hi, this is a dictionary management app with an offline, local storage based database.

Check out online demo: https://dicti.herokuapp.com

To run the app, please follow the next instructions:

(You should have node installed, to run the app locally.)

Clone the app:

`git clone https://rawplayer944@bitbucket.org/rawplayer944/dictionary_management.git`

Enter the directory:

`cd dictionary_management`

Install dependencies:

`npm i`

Run build script:

`npm run build`

Run the server on port 3000, while You are inside **dictionary_management** folder:

`node server.js`

Open URL **http://localhost:3000**, and You should have the app up and running.

##Todos:

.._Bulk delete management_..
.._Search bar on top of dashboard_..
.._Run tests to make dev complete with over 90% coverage_..
