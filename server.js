const express = require("express");
const path = require("path");

const app = express();

const PORT = process.env.PORT || 3000;
const root = `${__dirname}/build`;

app.use(express.static(root));

app.get("*", (req, res) => {
  res.status(404).sendFile(path.join(__dirname, "/build/404.html"));
});

app.listen(PORT);
